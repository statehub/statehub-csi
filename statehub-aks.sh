#!/bin/bash

BASE_DIR=`dirname $0`

echo "$0: $@"

# host node id
NODE_ID="${KUBE_NODE_IP//./_}"

# Make sure iSCSI_INITIATOR_NAME is defined
if [ -z "${iSCSI_INITIATOR_NAME}" ]; then
    iSCSI_INITIATOR_TAG="${NODE_ID}"
    iSCSI_INITIATOR_NAME="iqn.2018-02.io.statehub:${iSCSI_INITIATOR_TAG}"
fi

# Update initiatorname with ${iSCSI_INITIATOR_NAME}
echo "InitiatorName=${iSCSI_INITIATOR_NAME}" > /etc/iscsi/initiatorname.iscsi
cat /etc/iscsi/initiatorname.iscsi

# create scripts that intercept calls to mount and mkfs and redirect them to host binaries
echo -e '#!/bin/bash\n/host-sbin/iscsiadm $@' > /usr/local/sbin/iscsiadm
chmod +x /usr/local/sbin/iscsiadm

echo -e '#!/bin/bash\n/host-sbin/iscsid $@' > /usr/local/sbin/iscsid
chmod +x /usr/local/sbin/iscsid

# disable 'metadata_csum' feature of mkfs (unsupported by earlier distros)
echo "disable 'metadata_csum' for mkfs (default_features=^metadata_csum in /etc/mke2fs.conf)"
sed --in-place 's/.*\[defaults\].*/&\n\tdefault_features=^metadata_csum/' /etc/mke2fs.conf

# create scripts that intercept calls to mkfs and redirect them to host binaries
echo -e '#!/bin/bash\nLD_LIBRARY_PATH=/host-usr/lib /host-sbin/mkfs.xfs $@' > /usr/local/sbin/mkfs.xfs
chmod +x /usr/local/sbin/mkfs.xfs

echo -e '#!/bin/bash\nLD_LIBRARY_PATH=/host-usr/lib /host-sbin/fsck.xfs $@' > /usr/local/sbin/fsck.xfs
chmod +x /usr/local/sbin/fsck.xfs

# Start iscsid
echo "Start iscsid"
iscsid

# Start statehub-csi
/bin/statehub-csi $*
