#!/bin/bash

BASE_DIR=`dirname $0`

echo "$0: $*"

#TODO: better if
if [ $1 == "controller" ]; then
    echo "Controller mode"
else
    # host node id
    NODE_ID="${KUBE_NODE_IP//./_}"

    # Make sure iSCSI_INITIATOR_NAME is defined
    if [ -z "${iSCSI_INITIATOR_NAME}" ]; then
        iSCSI_INITIATOR_TAG="${NODE_ID}"
        iSCSI_INITIATOR_NAME="iqn.2018-02.io.statehub:${iSCSI_INITIATOR_TAG}"
    fi

    # Update initiatorname with ${iSCSI_INITIATOR_NAME}
    echo "InitiatorName=${iSCSI_INITIATOR_NAME}" > /etc/iscsi/initiatorname.iscsi
    cat /etc/iscsi/initiatorname.iscsi

    echo "Set initiatorname.iscsi"

    function quiet_insmod() {
        local mod_path="$1/$2"
        local action="Insert module: $2"
        msg=`insmod ${mod_path} 2>&1`
        if [[ $? == 0 ]]; then
            echo "${action} - OK $msg"
        else
            local err="${msg##*: }"
            if [ "${err}" == "File exists" ]; then
                echo "${action} - Already loaded"
            else
                echo "${action} - ERROR: ${err}"
                exit 1
            fi
        fi
    }

    # make sure iscsi kernel modules are loaded, from host fs
    HOST_MOD_PATH="/host-usr/lib/modules/`uname -r`/kernel/drivers/scsi"
    echo "Kernel modules dir: ${HOST_MOD_PATH}"
    quiet_insmod ${HOST_MOD_PATH} scsi_mod.ko
    quiet_insmod ${HOST_MOD_PATH} scsi_transport_iscsi.ko
    quiet_insmod ${HOST_MOD_PATH} libiscsi.ko
    quiet_insmod ${HOST_MOD_PATH} libiscsi_tcp.ko
    quiet_insmod ${HOST_MOD_PATH} iscsi_tcp.ko

    # create scripts that intercept calls to mount and mkfs and redirect them to host binaries
    echo -e '#!/bin/bash\nLD_LIBRARY_PATH=/host-usr/lib64 /host-usr/bin/mount $@' > /usr/local/sbin/mount
    chmod +x /usr/local/sbin/mount

    echo -e '#!/bin/bash\nMKE2FS_CONFIG=/dev/null LD_LIBRARY_PATH=/host-usr/lib64 /host-usr/sbin/mkfs.ext4 $@' > /usr/local/sbin/mkfs.ext4
    chmod +x /usr/local/sbin/mkfs.ext4

    echo -e '#!/bin/bash\nLD_LIBRARY_PATH=/host-usr/lib64 /host-usr/sbin/mkfs.xfs $@' > /usr/local/sbin/mkfs.xfs
    chmod +x /usr/local/sbin/mkfs.xfs

    echo -e '#!/bin/bash\nLD_LIBRARY_PATH=/host-usr/lib64 /host-usr/sbin/fsck.ext4 $@' > /usr/local/sbin/fsck.ext4
    chmod +x /usr/local/sbin/fsck.ext4

    echo -e '#!/bin/bash\nLD_LIBRARY_PATH=/host-usr/lib64 /host-usr/sbin/fsck.xfs $@' > /usr/local/sbin/fsck.xfs
    chmod +x /usr/local/sbin/fsck.xfs

    # Start iscsid
    echo "Start iscsid"
    iscsid -d 8 &> /tmp/iscsidlog
fi

# Start statehub-csi
/bin/statehub-csi $*
