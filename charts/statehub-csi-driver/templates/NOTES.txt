To verify that statehub-csi has started, run:

    kubectl get pod -n {{ .Release.Namespace }} -l "app.kubernetes.io/name={{ include "statehub-csi-driver.name" . }},app.kubernetes.io/instance={{ .Release.Name }}"
