#!/usr/bin/env bash

CMD=`basename "$0"`

HOST="/host"
if [ ! -d "${HOST}" ]; then
    echo "${HOST} Does not exist"
    exit 1
fi

exec chroot ${HOST} /usr/bin/env -i PATH="/sbin:/bin:/usr/bin" ${CMD} "${@:1}"
