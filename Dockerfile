# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#syntax=docker/dockerfile:1.2

FROM golang:1.16.9 AS builder
WORKDIR /go/src/gitlab.com/statehub/statehub-csi
COPY . .
RUN --mount=type=cache,target=/root/.cache/go-build make
RUN ./upx -5 bin/statehub-csi

FROM ubuntu:20.04 AS general
COPY --from=builder /go/src/gitlab.com/statehub/statehub-csi/bin/statehub-csi /bin/statehub-csi
ENTRYPOINT ["/bin/statehub-csi"]

###################
# generic #########
###################
FROM ubuntu:20.04 AS generic

LABEL maintainers="statehub.io"
LABEL description="Statehub CSI Plugin driver - (based on ubuntu 20.04)"

# Add packages
RUN apt-get update && apt-get -y install parted
#RUN yum -y update

# ARG DEBIAN_FRONTEND=noninteractive

# Copy statehub.sh - startup script
COPY statehub-generic.sh /statehub.sh

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

RUN mkdir /statehub

ADD host-wrapper.sh /statehub

RUN chmod a+x /statehub/host-wrapper.sh
RUN ln -s /statehub/host-wrapper.sh /statehub/iscsiadm \
    && ln -s /statehub/host-wrapper.sh /statehub/mkfs.ext4 \
    && ln -s /statehub/host-wrapper.sh /statehub/fsck.ext4 \
    && ln -s /statehub/host-wrapper.sh /statehub/mkfs.xfs \
    && ln -s /statehub/host-wrapper.sh /statehub/fsck.xfs \
    && ln -s /statehub/host-wrapper.sh /statehub/mount \
    && ln -s /statehub/host-wrapper.sh /statehub/udevadm

ENV PATH="/statehub:${PATH}"

COPY --from=builder /go/src/gitlab.com/statehub/statehub-csi/bin/statehub-csi /bin/statehub-csi

ENTRYPOINT ["/statehub.sh"]

###################
# AWS EKS #########
###################
FROM amazonlinux:2 AS eks

LABEL maintainers="statehub.io"
LABEL description="Statehub CSI Plugin driver for AWS EKS (based on Amazon Linux 2 )"


RUN yum -y install \
    initscripts \
    # iscsi initiator
    iscsi-initiator-utils \
    parted

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Copy startup script
COPY statehub-eks.sh /statehub.sh

COPY --from=builder /go/src/gitlab.com/statehub/statehub-csi/bin/statehub-csi /bin/statehub-csi

ENTRYPOINT ["/statehub.sh"]

###################
# Azure AKS #########
###################
FROM ubuntu:18.04 AS aks

LABEL maintainers="statehub.io"
LABEL description="Statehub CSI Plugin driver for Azure AKS (based on Ubuntu-18.04)"

# Add packages
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install open-iscsi parted
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY statehub-aks.sh /statehub.sh

COPY --from=builder /go/src/gitlab.com/statehub/statehub-csi/bin/statehub-csi /bin/statehub-csi

ENTRYPOINT ["/statehub.sh"]
