# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PKG=gitlab.com/statehub/statehub-csi
IMAGE?=registry.gitlab.com/statehub/statehub-csi
VERSION=0.0.34
VERSION_EKS=$(VERSION)-eks
VERSION_AKS=$(VERSION)-aks
GIT_COMMIT?=$(shell git rev-parse HEAD)
BUILD_DATE?=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
LDFLAGS?="-X ${PKG}/pkg/driver.driverVersion=${VERSION} -X ${PKG}/pkg/driver.gitCommit=${GIT_COMMIT} -X ${PKG}/pkg/driver.buildDate=${BUILD_DATE} -s -w"
GO111MODULE=on
GOPROXY=direct
GOPATH=$(shell go env GOPATH)
GOOS=$(shell go env GOOS)
GOBIN=$(shell pwd)/bin

.EXPORT_ALL_VARIABLES:

.PHONY: bin/statehub-csi
bin/statehub-csi: | bin
	if [ ! -d ./vendor ]; then (go mod tidy && go mod vendor); fi
	CGO_ENABLED=0 GOOS=linux go build -mod vendor -ldflags ${LDFLAGS} -o bin/statehub-csi ./cmd/

bin /tmp/helm /tmp/kubeval:
	@mkdir -p $@

bin/helm: | /tmp/helm bin
	@curl -o /tmp/helm/helm.tar.gz -sSL https://get.helm.sh/helm-v3.5.3-${GOOS}-amd64.tar.gz
	@tar -zxf /tmp/helm/helm.tar.gz -C bin --strip-components=1
	@rm -rf /tmp/helm/*

bin/kubeval: | /tmp/kubeval bin
	@curl -o /tmp/kubeval/kubeval.tar.gz -sSL https://github.com/instrumenta/kubeval/releases/download/0.15.0/kubeval-linux-amd64.tar.gz
	@tar -zxf /tmp/kubeval/kubeval.tar.gz -C bin kubeval
	@rm -rf /tmp/kubeval/*

bin/mockgen: | bin
	go get github.com/golang/mock/mockgen@v1.5.0

bin/golangci-lint: | bin
	echo "Installing golangci-lint..."
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s v1.21.0

.PHONY: kubeval
kubeval: bin/kubeval
	bin/kubeval -d deploy/kubernetes/base,deploy/kubernetes/cluster,deploy/kubernetes/overlays -i kustomization.yaml,crd_.+\.yaml,controller_add

mockgen: bin/mockgen
	./hack/update-gomock

.PHONY: verify
verify: bin/golangci-lint
	echo "verifying and linting files ..."
	./hack/verify-all
	echo "Congratulations! All Go source files have been linted."

.PHONY: test
test:
	go test -v -race ./cmd/... ./pkg/...

.PHONY: image-release
image-release:
	docker build -t $(IMAGE):$(VERSION) . --target general
	docker build -t $(IMAGE):$(VERSION_EKS) . --target eks

.PHONY: image
image: image-generic image-eks image-aks

.PHONY: image-eks
image-eks:
	docker build -t $(IMAGE):$(VERSION_EKS) . --target eks

image-aks:
	docker build -t $(IMAGE):$(VERSION_AKS) . --target aks

image-generic:
	docker build -t $(IMAGE):$(VERSION) . --target generic


.PHONY: push-release
push-release:
	docker push $(IMAGE):$(VERSION)
	docker push $(IMAGE):$(VERSION_EKS)
	docker push $(IMAGE):$(VERSION_AKS)

.PHONY: push
push: push-generic push-eks push-aks

push-generic:
	docker push $(IMAGE):$(VERSION)

.PHONY: push-eks
push-eks:
	docker push $(IMAGE):$(VERSION_EKS)

.PHONY: push-aks
push-aks:
	docker push $(IMAGE):$(VERSION_AKS)

.PHONY: clean-labels
clean-labels: | bin
	kubectl label nodes --all topology.statehub.csi/vnet- topology.statehub.csi/subnet- topology.statehub.csi/cloudProvider- topology.statehub.csi/zone- topology.statehub.csi/location- topology.statehub.csi/providerScope- topology.statehub.csi/provider- topology.statehub.csi/region- 1>/dev/null

.PHONY: example
create-block: | bin
	kubectl apply -f examples/kubernetes/dynamic-provisioning/specs/statefullset.yaml

helm-install: | bin
	helm install statehub-csi-driver ./charts/statehub-csi

helm-upgrade: | bin
	helm upgrade statehub-csi-driver ./charts/statehub-csi

helm-uninstall: | bin
	helm uninstall statehub-csi-driver

rollout-node: | bin
	kubectl rollout restart daemonset statehub-csi-node

rollout-controller: | bin
	kubectl rollout restart deployment statehub-csi-controller

rollout: rollout-node rollout-controller

example-random: | bin
	export RHOST=$$(kubectl get nodes | grep Ready | sort -R | head -n 1 | awk '{ print $$1 }'); \
	echo $$RHOST; \
	cat examples/kubernetes/dynamic-provisioning/specs/statefullset.yaml | sed "s@ip-.*@$$RHOST@" | kubectl apply -f -
