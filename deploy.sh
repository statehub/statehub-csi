#!/bin/bash

set -x

HELMS=$(curl -s "${INVENTORY_URL}/graphql" \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    -H "Origin: $INVENTORY_URL" \
    --data-binary "{\"query\":\"{\n  helms (find: { chart: \\\"$CHART\\\" }){\n    id\n provider v }\n}\n\"}" \
    --compressed | jq -r -c '.data.helms[]')

echo $HELMS

CHART_VERSION=$(cat ./charts/${CHART}/Chart.yaml | grep version: | awk '{print $2}')

for HELM in $HELMS; do
    echo "helm: $HELM"
    HELM_ID=$(echo $HELM | jq -r '.id')
    HELM_PROVIDER=$(echo $HELM | jq -r '.provider')
    ENTRY_VERSION=$(echo $HELM | jq -r '.v')
    echo $HELM_PROVIDER

    PARAMS="[]"
    PARAMS="[{key: \\\"provider\\\", value: \\\"generic\\\"}]"

    case $HELM_PROVIDER in

    AKS)
        PARAMS="[{key: \\\"provider\\\", value: \\\"aks\\\"}]"
        ;;

    EKS)
        PARAMS="[{key: \\\"provider\\\", value: \\\"eks\\\"}]"
        ;;
    esac

    echo "parameters: $PARAMS"

    echo "Entry version: $ENTRY_VERSION"

    curl -s "${INVENTORY_URL}/graphql" \
        -H 'Accept-Encoding: gzip, deflate, br' \
        -H 'Content-Type: application/json' \
        -H 'Accept: application/json' \
        -H 'Connection: keep-alive' \
        -H 'DNT: 1' \
        -H "Origin: $INVENTORY_URL" \
        --data-binary "{\"query\":\"mutation {\n  updateHelm(\n    find: { id: \\\"$HELM_ID\\\", v: $ENTRY_VERSION }\n    input: { version: \\\"$CHART_VERSION\\\" parameters: $PARAMS }\n  ) {\n    id\n  }\n}\"}" \
        --compressed
done