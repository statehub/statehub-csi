/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package statehub

import (
	"fmt"

	cloudProviderPkg "gitlab.com/statehub/statehub-cloud/pkg/cloud"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/klog"
)

type Metadata struct {
	InstanceID       string
	Region           string
	AvailabilityZone string
	Vnet             string
	Subnet           string
	CloudProvider    string
	ProviderScope    string
	ResourceGroup    string
}

var _ MetadataService = &Metadata{}

// GetInstanceID returns the instance identification.
func (m *Metadata) GetInstanceID() string {
	return m.InstanceID
}

// GetLocation returns the region which the instance is in.
func (m *Metadata) GetRegion() string {
	return m.Region
}

// GetAvailabilityZone returns the Availability Zone which the instance is in.
func (m *Metadata) GetAvailabilityZone() string {
	return m.AvailabilityZone
}

// GetVnet returns node's vnet
func (m *Metadata) GetVnet() string {
	return m.Vnet
}

// GetSubnet returns node's subnet
func (m *Metadata) GetSubnet() string {
	return m.Subnet
}

// GetProviderScope returns an aws account id or azure subscription id
func (m *Metadata) GetProviderScope() string {
	return m.ProviderScope
}

// GetProviderScope returns an aws account id or azure subscription id
func (m *Metadata) GetResourceGroup() string {
	return m.ResourceGroup
}

// GetCloudProvider returns node's provider
func (m *Metadata) GetCloudProvider() string {
	return m.CloudProvider
}

func NewMetadata() (MetadataService, error) {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("error creating in-cluster config: %w", err)
	}
	klog.V(5).Info("[DEBUG] NewMetadata: Created in-cluster config")
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("error creating clientset: %w", err)
	}
	klog.V(5).Info("[DEBUG] NewMetadata: Created clientset")
	metadataService, err := NewMetadataService(clientset)
	if err != nil {
		return nil, fmt.Errorf("error getting information from metadata service or node object: %w", err)
	}
	return metadataService, err
}

// NewMetadataService returns a new MetadataServiceImplementation.
func NewMetadataService(clientset kubernetes.Interface) (MetadataService, error) {
	// TODO: ebs-csi has a better way to get aws metadata, we should use it instead

	klog.V(5).Infof("[DEBUG] NewMetadataService: Will now identify cloud")
	cloudInstace, err := cloudProviderPkg.GetCloud()
	if err != nil {
		return nil, fmt.Errorf("[Error] NewMetadataService: %v", err)
	}
	cloudProvider := string(cloudInstace.GetProvider())
	klog.V(5).Infof("[DEBUG] NewMetadataService: Identified cloud %s", cloudProvider)

	cloudInstace.LoadCredentials()

	instanceMetadata, err := cloudInstace.GetMetadata()
	if err != nil {
		return nil, fmt.Errorf("[Error] NewMetadataService: %v", err)
	}

	metadata := Metadata{
		InstanceID:       instanceMetadata.InstanceID,
		Region:           instanceMetadata.Region,
		AvailabilityZone: instanceMetadata.AvailabilityZone,
		CloudProvider:    cloudProvider,
		Vnet:             instanceMetadata.Vnet,
		Subnet:           instanceMetadata.Subnet,
		ProviderScope:    instanceMetadata.ProviderScope,
		ResourceGroup:    instanceMetadata.ResourceGroup,
	}

	return &metadata, nil
}
