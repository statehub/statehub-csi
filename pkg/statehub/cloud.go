/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package statehub

import (
	"context"
	"fmt"
	"os"
	"time"

	"net/http"
	"net/url"

	statehubAPI "gitlab.com/statehub/openapi-go"
	"gitlab.com/statehub/statehub-csi/pkg/util"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/klog"
)

// Defaults
const (
	// DefaultVolumeSize represents the default volume size.
	DefaultVolumeSize int64 = 1 * util.GiB
)

// DiskOptions represents parameters to create a state's volume
type DiskOptions struct {
	CapacityBytes int64
	VolumeType    string
}

type statehub struct {
	statehubClient *statehubAPI.APIClient
}

var _ StateHub = &statehub{}

func NewStatehub(statehubapi string) (StateHub, error) {
	config := statehubAPI.NewConfiguration()
	uri, err := url.ParseRequestURI(statehubapi)
	if err != nil {
		return nil, err
	}
	config.Host = uri.Host
	config.Scheme = uri.Scheme
	// tr := &http.Transport{
	// 	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	// }
	// client := &http.Client{Transport: tr, Timeout: 8 * time.Second}
	client := &http.Client{Timeout: 8 * time.Second}
	config.HTTPClient = client
	config.Debug = true
	config.AddDefaultHeader("Authorization", fmt.Sprint("Bearer ", os.Getenv("STATEHUB_TOKEN")))
	statehubClient := statehubAPI.NewAPIClient(config)

	return &statehub{
		statehubClient: statehubClient,
	}, nil
}

func (c *statehub) CreateVolume(ctx context.Context, volumeOptions *statehubAPI.CreateVolumeDto, state string) (*statehubAPI.Volume, error) {

	existVolume, err := c.GetVolumeByName(ctx, state, volumeOptions.Name)
	klog.V(5).Infof("CreateVolume: recived existVolume: %w", existVolume)
	if err != nil {
		switch err.Error() {
		case "404 Not Found":
			klog.V(5).Info("CreateVolume: recived 404")
			request := c.statehubClient.VolumesApi.VolumesControllerCreate(ctx, state)
			request = request.CreateVolumeDto(*volumeOptions)
			volume, _, err := request.Execute()
			return &volume, err
		case "403 Forbidden":
			klog.V(5).Info("CreateVolume: recived 403")
			return nil, fmt.Errorf("cluster has no permission to get volumes in state %s", state)
		default:
			klog.V(5).Infof("CreateVolume: recived unknown error: %s", err.Error())
			return nil, err
		}
	}

	if existVolume == nil {
		klog.V(5).Info("CreateVolume: recived nil existing volume, will create new")
		request := c.statehubClient.VolumesApi.VolumesControllerCreate(ctx, state)
		request = request.CreateVolumeDto(*volumeOptions)
		volume, _, err := request.Execute()
		if err != nil {
			switch err.Error() {
			case "403 Forbidden":
				klog.V(5).Info("CreateVolume: recived 403")
				return nil, fmt.Errorf("cluster has no permission to create volumes in state %s", state)
			default:
				klog.V(5).Infof("CreateVolume: recived unknown error: %s", err.Error())
				return nil, err
			}
		}
		return &volume, err
	}

	if !compareVolume(existVolume, volumeOptions) {
		// Volume by this name already exists, return an error
		return nil, fmt.Errorf("CreateVolume: volume by name %s already exists", volumeOptions.Name)
	}

	return existVolume, nil
}

func (c *statehub) DeleteDisk(ctx context.Context, volumeName string) (bool, error) {
	// TODO: implement this
	return true, nil
}

func (c *statehub) GetVolumeByName(ctx context.Context, state string, volumeName string) (*statehubAPI.Volume, error) {
	volume, err := c.getVolume(ctx, state, volumeName)

	if err != nil {
		return nil, err
	}

	return volume, nil
}

func (c *statehub) getVolume(ctx context.Context, state string, volumeName string) (*statehubAPI.Volume, error) {
	klog.V(5).Infof("getVolume: getting volume %s", volumeName)
	request := c.statehubClient.VolumesApi.VolumesControllerFindOne(ctx, state, volumeName)
	volume, _, err := request.Execute()
	klog.V(5).Infof("getVolume: got volume %w", volume)
	return &volume, err
}

// waitForVolume waits for volume to be in the "available" state.
func (c *statehub) waitForVolume(ctx context.Context, volumeID string) error {
	var (
		checkInterval = 3 * time.Second
		// This timeout can be "ovewritten" if the value returned by ctx.Deadline()
		// comes sooner. That value comes from the external provisioner controller.
		checkTimeout = 1 * time.Minute
	)

	err := wait.Poll(checkInterval, checkTimeout, func() (done bool, err error) {
		//TODO: check volume status
		/*vol, err := c.getVolume(ctx, request)
		if err != nil {
			return true, err
		}
		if vol.State != nil {
			return *vol.State == "available", nil
		}*/
		return false, nil
	})

	return err
}

func (c *statehub) ResizeDisk(ctx context.Context, volumeName string, reqSize int32) (newSize int32, err error) {
	//TODO: implement this
	return reqSize, nil
}

func compareVolume(existVolume *statehubAPI.Volume, volumeOptions *statehubAPI.CreateVolumeDto) bool {
	if volumeOptions.Name != existVolume.Name {
		return false
	} else if volumeOptions.FsType != existVolume.FsType {
		return false
	} else if volumeOptions.SizeGi != existVolume.SizeGi {
		return false
	}

	return true
}

func (c *statehub) CheckIfOwner(ctx context.Context, state string) (bool, error) {
	getStateRequest := c.statehubClient.StatesApi.StatesControllerFindOne(ctx, state)
	stateObject, _, err := getStateRequest.Execute()
	if err != nil {
		return false, err
	}
	getClusterRequest := c.statehubClient.ClustersApi.ClustersControllerFindMany(ctx)
	clustersObjects, _, err := getClusterRequest.Execute()
	if err != nil {
		return false, err
	}

	klog.V(5).Infof("I am cluster %+v and checking if im %+v", clustersObjects[0].Name, *stateObject.Owner)

	return *stateObject.Owner == clustersObjects[0].Name, nil
}

func (c *statehub) SetActiveLocation(ctx context.Context, volumeName string, state string, cloud string, region string) error {
	getVolumeRequest := c.statehubClient.VolumesApi.VolumesControllerFindOne(ctx, state, volumeName)
	volume, _, err := getVolumeRequest.Execute()
	if err != nil {
		return err
	}
	if volume.ActiveLocation == nil {
		klog.V(5).Infof("did not find active location on volume %s in state %s", volumeName, state)
		setActiveLocationRequest := c.statehubClient.VolumeActiveLocationApi.VolumeActiveLocationControllerSet(ctx, state, volumeName, cloud, region)
		_, _, err := setActiveLocationRequest.Execute()
		return err
	}
	if *volume.ActiveLocation != fmt.Sprint(cloud, "/", region) {
		setActiveLocationRequest := c.statehubClient.VolumeActiveLocationApi.VolumeActiveLocationControllerSet(ctx, state, volumeName, cloud, region)
		_, _, err := setActiveLocationRequest.Execute()
		return err
	}
	return nil
}

func (c *statehub) FromatVolume(ctx context.Context, state string, volumeName string) error {
	req := c.statehubClient.VolumesApi.VolumesControllerUpdateFormatted(ctx, state, volumeName)
	_, _, err := req.Execute()
	return err
}

func (c *statehub) SendEvent(ctx context.Context, event map[string]interface{}) error {
	req := c.statehubClient.EventsApi.EventsControllerCreate(ctx)

	name := "k8s-csi"
	chartVersion := "1.0.27"

	req = req.CreateEventDto(statehubAPI.CreateEventDto{
		Reporter: statehubAPI.EventActorComponent{
			Name:    &name,
			Version: &chartVersion,
		},
		Event: event,
	})

	_, err := req.Execute()
	return err
}
