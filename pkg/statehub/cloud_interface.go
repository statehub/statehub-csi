/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package statehub

import (
	"context"

	statehubAPI "gitlab.com/statehub/openapi-go"
)

type StateHub interface {
	CreateVolume(ctx context.Context, volumeOptions *statehubAPI.CreateVolumeDto, state string) (*statehubAPI.Volume, error)
	DeleteDisk(ctx context.Context, volumeName string) (success bool, err error)
	ResizeDisk(ctx context.Context, volumeName string, reqSize int32) (newSize int32, err error)
	GetVolumeByName(ctx context.Context, state string, volumeName string) (*statehubAPI.Volume, error)
	CheckIfOwner(ctx context.Context, state string) (bool, error)
	SetActiveLocation(ctx context.Context, volumeName string, state string, cloud string, region string) error
	FromatVolume(ctx context.Context, state string, volumeName string) error
	SendEvent(ctx context.Context, event map[string]interface{}) error
}

// MetadataService represents multi-cloud metadata service.
type MetadataService interface {
	GetInstanceID() string
	GetRegion() string
	GetAvailabilityZone() string
	GetVnet() string
	GetSubnet() string
	GetCloudProvider() string
	GetProviderScope() string
	GetResourceGroup() string
}
