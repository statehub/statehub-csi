package mounter

import (
	"fmt"
	"os"
	"regexp"

	"context"

	"github.com/container-storage-interface/spec/lib/go/csi"
	"k8s.io/klog"
	"k8s.io/utils/exec"
	"k8s.io/utils/mount"
)

type DeviceMounter struct {
	fsType       string
	mountOptions []string
	mounter      *mount.SafeFormatAndMount
	targetPath   string
	ShouldFormat bool
	devicePath   string
	EventLogger  func(ctx context.Context, event map[string]interface{}) error
	volumeID     string
}

type DeviceUmounter struct {
	mounter *mount.SafeFormatAndMount
}

func GetDeviceMounterStage(req *csi.NodeStageVolumeRequest, devicePath string) *DeviceMounter {
	fsType := req.GetVolumeCapability().GetMount().GetFsType()
	mountOptions := req.GetVolumeCapability().GetMount().GetMountFlags()

	return &DeviceMounter{
		fsType:       fsType,
		mountOptions: mountOptions,
		mounter:      &mount.SafeFormatAndMount{Interface: mount.New(""), Exec: exec.New()},
		targetPath:   req.GetStagingTargetPath(),
		ShouldFormat: false, // By default do not format
		devicePath:   devicePath,
		volumeID:     req.GetVolumeId(),
	}
}

func GetDeviceMounterPublish(req *csi.NodePublishVolumeRequest, source string, target string) *DeviceMounter {
	fsType := req.GetVolumeCapability().GetMount().GetFsType()
	mountOptions := req.GetVolumeCapability().GetMount().GetMountFlags()

	if req.GetReadonly() {
		mountOptions = append(mountOptions, "ro")
	} else {
		mountOptions = append(mountOptions, "rw")
	}
	mountOptions = append(mountOptions, "bind")

	klog.V(4).Infof("GetDeviceMounterPublish mountOptions: %+v", mountOptions)

	return &DeviceMounter{
		fsType:       fsType,
		mountOptions: mountOptions,
		mounter:      &mount.SafeFormatAndMount{Interface: mount.New(""), Exec: exec.New()},
		targetPath:   target,
		ShouldFormat: false, // By default do not format
		devicePath:   source,
	}
}

func GetDeviceUmounter() *DeviceUmounter {
	return &DeviceUmounter{
		mounter: &mount.SafeFormatAndMount{Interface: mount.New(""), Exec: exec.New()},
	}
}

func MountStaging(b *DeviceMounter, ctx context.Context) error {
	var options []string
	// TODO: check if needed to set ro here or in NodePublishVolume
	// if b.readOnly {
	// 	options = append(options, "ro")
	// } else {
	// 	options = append(options, "rw")
	// }

	// Making sure that the device is ready
	args := []string{"settle"}
	_, err := b.mounter.Exec.Command("udevadm", args...).CombinedOutput()
	if err != nil {
		klog.Errorf("mounting: failed to udevadm error %+v", err)
		return err
	}

	args = []string{b.devicePath}
	_, err = b.mounter.Exec.Command("partprobe", args...).CombinedOutput()
	if err != nil {
		klog.Errorf("mounting: failed to partprobe error %+v", err)
		return err
	}

	args = []string{"settle"}
	_, err = b.mounter.Exec.Command("udevadm", args...).CombinedOutput()
	if err != nil {
		klog.Errorf("mounting: failed to second udevadm error %+v", err)
		return err
	}

	mntPath := b.targetPath // Mounting to stage location

	// We do not support block devices at this point
	if b.fsType != "block" { // mount a file system volume
		if err := os.MkdirAll(mntPath, 0750); err != nil {
			klog.Errorf("iscsi: failed to mkdir %s, error", mntPath)
			return err
		}

		options = append(options, b.mountOptions...)

		if b.ShouldFormat {
			err = b.mounter.FormatAndMount(b.devicePath, mntPath, b.fsType, options)
		} else {
			if b.fsType == "ext4" {
				err = checkAndRepairFilesystem(b.devicePath, b.mounter)
				if err == nil {
					event := make(map[string]interface{})
					event["function"] = "MountStaging"
					event["driver"] = "node"
					event["volume"] = b.volumeID
					event["type"] = "ok"
					event["data"] = "fsck returned no issues"
					reqError := b.EventLogger(ctx, event)
					if reqError != nil {
						klog.Warningf("MountStaging: failed to send event, error %v", reqError)
					}
					err = b.mounter.Mount(b.devicePath, mntPath, b.fsType, options)
				} else {
					event := make(map[string]interface{})
					event["function"] = "MountStaging"
					event["driver"] = "node"
					event["volume"] = b.volumeID
					event["type"] = "error"
					event["data"] = err.Error()
					reqError := b.EventLogger(ctx, event)
					if reqError != nil {
						klog.Warningf("MountStaging: failed to send fsck error event, error %v", reqError)
					}
				}
			} else {
				err = b.mounter.Mount(b.devicePath, mntPath, b.fsType, options)
			}
		}

		if err != nil {
			event := make(map[string]interface{})
			event["function"] = "MountStaging"
			event["driver"] = "node"
			event["volume"] = b.volumeID
			event["type"] = "error"
			event["data"] = err.Error()
			reqError := b.EventLogger(ctx, event)
			if reqError != nil {
				klog.Warningf("MountStaging: failed to send event, error %v", reqError)
			}
			klog.Errorf("iscsi: failed to mount iscsi volume %s [%s] to %s, error %v", b.devicePath, b.fsType, mntPath, err)
			return err
		}
		event := make(map[string]interface{})
		event["function"] = "MountStaging"
		event["driver"] = "node"
		event["volume"] = b.volumeID
		event["type"] = "ok"
		event["data"] = "mounted"
		reqError := b.EventLogger(ctx, event)
		if reqError != nil {
			klog.Warningf("MountStaging: failed to send event, error %v", reqError)
		}
	} else { // bind mount a block device
		// options = append(options, "bind")

		// // Check if the target path exists. Create if not present.
		// _, err = os.Lstat(mntPath)
		// if os.IsNotExist(err) {
		// 	if err = createFile(mntPath); err != nil {
		// 		return "", fmt.Errorf("failed to create mount path: %s: %v", mntPath, err)
		// 	}
		// }
		// if err != nil {
		// 	return "", fmt.Errorf("failed to check if the target block file exists: %v", err)
		// }

		// if err = b.mounter.Mount(devicePath, mntPath, "", options); err != nil {
		// 	return "", fmt.Errorf("failed to mount block device: %s at %s: %v",
		// 		devicePath, mntPath, err)
		// }
		return fmt.Errorf("block type is not supported")
	}

	return nil
}

func MountPublish(b *DeviceMounter) error {
	mntPath := b.targetPath // Mounting to publish location

	if err := os.MkdirAll(mntPath, 0750); err != nil {
		klog.Errorf("MountPublish: failed to mkdir %s, error", mntPath)
		return err
	}

	err := b.mounter.Mount(b.devicePath, mntPath, b.fsType, b.mountOptions)

	if err != nil {
		klog.Errorf("MountPublish: failed to mount volume %s [%s] to %s, error %v", b.devicePath, b.fsType, mntPath, err)
		return err
	}

	return nil
}

func UmountStaging(c *DeviceUmounter, targetPath string) error {
	_, cnt, err := mount.GetDeviceNameFromMount(c.mounter, targetPath)
	if err != nil {
		klog.Errorf("umount: failed to get device from mnt: %s\nError: %v", targetPath, err)
		return err
	}

	if pathExists, pathErr := mount.PathExists(targetPath); pathErr != nil {
		return fmt.Errorf("Error checking if path exists: %v", pathErr)
	} else if !pathExists {
		klog.Warningf("Warning: Unmount skipped because path does not exist: %v", targetPath)
		return nil
	}
	if err = c.mounter.Unmount(targetPath); err != nil {
		checkIfNotMountedError, regexerr := regexp.Compile(`(?s)^unmount failed.*exit status 32.*not mounted\.`)
		if regexerr != nil {
			klog.Errorf("umount: failed to create regex for path: %s\nError: %v", targetPath, err)
			return regexerr
		}
		if !checkIfNotMountedError.MatchString(err.Error()) { // If error is different, return the error
			klog.Errorf("umount: failed to unmount: %s\nError: %v", targetPath, err)
			return err
		}
	}
	cnt--
	if cnt != 0 {
		return nil
	}

	return nil
}

const (
	// 'fsck' found errors and corrected them
	fsckErrorsCorrected = 1
	// 'fsck' found errors but exited without correcting them
	fsckErrorsUncorrected = 4
)

func checkAndRepairFilesystem(source string, mounter *mount.SafeFormatAndMount) error {
	klog.V(4).Infof("Checking for issues with fsck on disk: %s", source)
	args := []string{"-a", source}
	out, err := mounter.Exec.Command("fsck.ext4", args...).CombinedOutput()
	if err != nil {
		ee, isExitError := err.(exec.ExitError)
		switch {
		case err == exec.ErrExecutableNotFound:
			klog.Warningf("'fsck' not found on system; continuing mount without running 'fsck'.")
		case isExitError && ee.ExitStatus() == fsckErrorsCorrected:
			klog.Infof("Device %s has errors which were corrected by fsck.", source)
		case isExitError && ee.ExitStatus() == fsckErrorsUncorrected:
			klog.Errorf("'fsck' found errors on device %s but could not correct them: %s", source, string(out))
		case isExitError && ee.ExitStatus() > fsckErrorsUncorrected:
			klog.Infof("`fsck` error %s", string(out))
		}
		return err
	}
	return nil
}
