/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package iscsi

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/container-storage-interface/spec/lib/go/csi"
	iscsiLib "github.com/kubernetes-csi/csi-lib-iscsi/iscsi"
	"k8s.io/kubernetes/pkg/volume/util"
	"k8s.io/utils/exec"
	"k8s.io/utils/mount"
)

func GetISCSIInfo(req *csi.NodeStageVolumeRequest) (*iscsiDisk, error) {
	iscsiLib.EnableDebugLogging(os.Stdout)
	volName := req.GetVolumeId()
	tp := req.GetVolumeContext()["targetPortal"]
	iqn := req.GetVolumeContext()["iqn"]
	lun := req.GetVolumeContext()["lun"]
	if tp == "" || iqn == "" || lun == "" {
		return nil, fmt.Errorf("iSCSI target information is missing")
	}

	portal := portalMounter(tp)
	var bkportal []string
	bkportal = append(bkportal, portal)

	chapDiscovery := false
	if req.GetVolumeContext()["discoveryCHAPAuth"] == "true" {
		chapDiscovery = true
	}

	chapSession := false
	if req.GetVolumeContext()["sessionCHAPAuth"] == "true" {
		chapSession = true
	}

	var chapUser string
	if req.GetVolumeContext()["chapUser"] != "" {
		chapUser = req.GetVolumeContext()["chapUser"]
	}

	var chapPassword string
	if req.GetVolumeContext()["chapPassword"] != "" {
		chapPassword = req.GetVolumeContext()["chapPassword"]
	}

	var lunVal int32
	if lun != "" {
		l, err := strconv.Atoi(lun)
		if err != nil {
			return nil, err
		}
		lunVal = int32(l)
	}

	return &iscsiDisk{
		VolName:       volName,
		Portals:       bkportal,
		Iqn:           iqn,
		lun:           lunVal,
		chapDiscovery: chapDiscovery,
		chapSession:   chapSession,
		chapUser:      chapUser,
		chapPassword:  chapPassword,
	}, nil
}

func buildISCSIConnector(iscsiInfo *iscsiDisk) *iscsiLib.Connector {
	var chapSecrets iscsiLib.Secrets = iscsiLib.Secrets{}
	if iscsiInfo.chapUser != "" {
		chapSecrets = iscsiLib.Secrets{
			SecretsType: "chap",
			UserName:    iscsiInfo.chapUser,
			Password:    iscsiInfo.chapPassword,
		}
	}

	c := iscsiLib.Connector{
		VolumeName:    iscsiInfo.VolName,
		TargetIqn:     iscsiInfo.Iqn,
		TargetPortals: iscsiInfo.Portals,
		Multipath:     len(iscsiInfo.Portals) > 1,
		Lun:           iscsiInfo.lun,
		// DiscoverySecrets: chapSecrets,
		SessionSecrets: chapSecrets,
	}

	return &c
}

func GetISCSIDiskMounter(iscsiInfo *iscsiDisk, req *csi.NodeStageVolumeRequest) *iscsiDiskMounter {
	fsType := req.GetVolumeCapability().GetMount().GetFsType()
	mountOptions := req.GetVolumeCapability().GetMount().GetMountFlags()

	return &iscsiDiskMounter{
		iscsiDisk:    iscsiInfo,
		fsType:       fsType,
		mountOptions: mountOptions,
		mounter:      &mount.SafeFormatAndMount{Interface: mount.New(""), Exec: exec.New()},
		exec:         exec.New(),
		targetPath:   req.GetStagingTargetPath(),
		deviceUtil:   util.NewDeviceHandler(util.NewIOHandler()),
		connector:    buildISCSIConnector(iscsiInfo),
	}
}

func GetISCSIDiskUnmounter(req *csi.NodeUnstageVolumeRequest) *iscsiDiskUnmounter {
	return &iscsiDiskUnmounter{
		iscsiDisk: &iscsiDisk{
			VolName: req.GetVolumeId(),
		},
		mounter: mount.New(""),
		exec:    exec.New(),
	}
}

func portalMounter(portal string) string {
	if !strings.Contains(portal, ":") {
		portal = portal + ":3260"
	}
	return portal
}

func parseSecret(secretParams string) map[string]string {
	var secret map[string]string
	if err := json.Unmarshal([]byte(secretParams), &secret); err != nil {
		return nil
	}
	return secret
}

func parseSessionSecret(secretParams map[string]string) (iscsiLib.Secrets, error) {
	var ok bool
	secret := iscsiLib.Secrets{}

	if len(secretParams) == 0 {
		return secret, nil
	}

	if secret.UserName, ok = secretParams["node.session.auth.username"]; !ok {
		return iscsiLib.Secrets{}, fmt.Errorf("node.session.auth.username not found in secret")
	}
	if secret.Password, ok = secretParams["node.session.auth.password"]; !ok {
		return iscsiLib.Secrets{}, fmt.Errorf("node.session.auth.password not found in secret")
	}
	if secret.UserNameIn, ok = secretParams["node.session.auth.username_in"]; !ok {
		return iscsiLib.Secrets{}, fmt.Errorf("node.session.auth.username_in not found in secret")
	}
	if secret.PasswordIn, ok = secretParams["node.session.auth.password_in"]; !ok {
		return iscsiLib.Secrets{}, fmt.Errorf("node.session.auth.password_in not found in secret")
	}

	secret.SecretsType = "chap"
	return secret, nil
}

func parseDiscoverySecret(secretParams map[string]string) (iscsiLib.Secrets, error) {
	var ok bool
	secret := iscsiLib.Secrets{}

	if len(secretParams) == 0 {
		return secret, nil
	}

	if secret.UserName, ok = secretParams["node.sendtargets.auth.username"]; !ok {
		return iscsiLib.Secrets{}, fmt.Errorf("node.sendtargets.auth.username not found in secret")
	}
	if secret.Password, ok = secretParams["node.sendtargets.auth.password"]; !ok {
		return iscsiLib.Secrets{}, fmt.Errorf("node.sendtargets.auth.password not found in secret")
	}
	if secret.UserNameIn, ok = secretParams["node.sendtargets.auth.username_in"]; !ok {
		return iscsiLib.Secrets{}, fmt.Errorf("node.sendtargets.auth.username_in not found in secret")
	}
	if secret.PasswordIn, ok = secretParams["node.sendtargets.auth.password_in"]; !ok {
		return iscsiLib.Secrets{}, fmt.Errorf("node.sendtargets.auth.password_in not found in secret")
	}

	secret.SecretsType = "chap"
	return secret, nil
}

type iscsiDisk struct {
	Portals       []string
	Iqn           string
	lun           int32
	chapDiscovery bool
	chapSession   bool
	VolName       string
	chapUser      string
	chapPassword  string
}

type iscsiDiskMounter struct {
	*iscsiDisk
	fsType       string
	mountOptions []string
	mounter      *mount.SafeFormatAndMount
	exec         exec.Interface
	deviceUtil   util.DeviceUtil
	targetPath   string
	connector    *iscsiLib.Connector
}

type iscsiDiskUnmounter struct {
	*iscsiDisk
	mounter mount.Interface
	exec    exec.Interface
}
