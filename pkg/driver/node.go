/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package driver

import (
	"context"
	"encoding/json"
	"fmt"

	csi "github.com/container-storage-interface/spec/lib/go/csi"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"k8s.io/klog"

	"gitlab.com/statehub/openapi-go"
	statehubTypes "gitlab.com/statehub/statehub-cloud/pkg/types"
	"gitlab.com/statehub/statehub-csi/pkg/driver/internal"
	"gitlab.com/statehub/statehub-csi/pkg/iscsi"
	"gitlab.com/statehub/statehub-csi/pkg/mounter"
	"gitlab.com/statehub/statehub-csi/pkg/statehub"
)

const (
	// FSTypeExt2 represents the ext2 filesystem type
	FSTypeExt2 = "ext2"
	// FSTypeExt3 represents the ext3 filesystem type
	FSTypeExt3 = "ext3"
	// FSTypeExt4 represents the ext4 filesystem type
	FSTypeExt4 = "ext4"
	// FSTypeXfs represents te xfs filesystem type
	FSTypeXfs = "xfs"

	// default file system type to be used when it is not provided
	defaultFsType = FSTypeExt4

	// defaultMaxVolumes is the maximum number of volumes that an instance can have attached.
	defaultMaxVolumes = 39

	// VolumeOperationAlreadyExists is message fmt returned to CO when there is another in-flight call on the given volumeID
	VolumeOperationAlreadyExists = "An operation with the given volume=%q is already in progress"
)

var (
	ValidFSTypes = []string{FSTypeExt2, FSTypeExt3, FSTypeExt4, FSTypeXfs}
)

var (
	// nodeCaps represents the capability of node service.
	nodeCaps = []csi.NodeServiceCapability_RPC_Type{
		csi.NodeServiceCapability_RPC_STAGE_UNSTAGE_VOLUME,
		//csi.NodeServiceCapability_RPC_GET_VOLUME_STATS,
	}
)

// nodeService represents the node service of CSI driver
type nodeService struct {
	metadata      statehub.MetadataService
	inFlight      *internal.InFlight
	driverOptions *DriverOptions
	statehub      statehub.StateHub
}

// newNodeService creates a new node service
// it panics if failed to create the service
func newNodeService(driverOptions *DriverOptions) nodeService {
	klog.V(5).Infof("[Debug] Retrieving node info from metadata service")

	metadata, err := statehub.NewMetadata()
	if err != nil {
		klog.Errorf("[ERROR] Retrieving node info from metadata service failed to error %+v", err)
		panic(err)
	}

	statehubSrv, err := NewCloudFunc(driverOptions.statehubapi)
	if err != nil {
		panic(err)
	}

	return nodeService{
		metadata:      metadata,
		inFlight:      internal.NewInFlight(),
		driverOptions: driverOptions,
		statehub:      statehubSrv,
	}
}

func (d *nodeService) NodeStageVolume(ctx context.Context, req *csi.NodeStageVolumeRequest) (*csi.NodeStageVolumeResponse, error) {
	klog.V(4).Infof("NodeStageVolume: called with args %+v", *req)

	volumeID := req.GetVolumeId()
	if len(volumeID) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Volume ID not provided")
	}

	if ok := d.inFlight.Insert(volumeID); !ok {
		return nil, status.Errorf(codes.Aborted, VolumeOperationAlreadyExists, volumeID)
	}
	defer func() {
		klog.V(4).Infof("NodeStageVolume: volume=%q operation finished", volumeID)
		d.inFlight.Delete(volumeID)
	}()

	// Check ownership of cluster

	state := req.VolumeContext["state"]

	isOwner, err := d.statehub.CheckIfOwner(ctx, state)
	if err != nil {
		errString := fmt.Sprint("Recived error from CheckIfOwner ", err)
		return nil, status.Error(codes.Internal, errString)
	}
	if !isOwner {
		errString := "Not owner of state " + state + ". Change state's owner to allow this cluster access volumes on state."
		return nil, status.Error(codes.PermissionDenied, errString)
	}

	// We are owners of the cluster. Let's set active location for this node.

	err = d.statehub.SetActiveLocation(ctx, volumeID, state, d.metadata.GetCloudProvider(), d.metadata.GetRegion())
	if err != nil {
		errString := fmt.Sprint("Recived error from SetActiveLocation ", err)
		return nil, status.Error(codes.Internal, errString)
	}

	// Prepare for staging

	target := req.GetStagingTargetPath()
	if len(target) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Staging target not provided")
	}

	volCap := req.GetVolumeCapability()
	if volCap == nil {
		return nil, status.Error(codes.InvalidArgument, "Volume capability not provided")
	}

	if !isValidVolumeCapabilities([]*csi.VolumeCapability{volCap}) {
		return nil, status.Error(codes.InvalidArgument, "Volume capability not supported")
	}
	volumeContext := req.GetVolumeContext()
	if isValidVolumeContext := isValidVolumeContext(volumeContext); !isValidVolumeContext {
		return nil, status.Error(codes.InvalidArgument, "Volume Attribute is not valid")
	}

	// Do not support block
	switch volCap.GetAccessType().(type) {
	case *csi.VolumeCapability_Block:
		return nil, status.Error(codes.InvalidArgument, "Block is not supported")
	}

	mountVolume := volCap.GetMount()
	if mountVolume == nil {
		return nil, status.Error(codes.InvalidArgument, "NodeStageVolume: mount is nil within volume capability")
	}

	fsType := mountVolume.GetFsType()
	if len(fsType) == 0 {
		fsType = defaultFsType
	}

	var mountOptions []string
	for _, f := range mountVolume.MountFlags {
		if !hasMountOption(mountOptions, f) {
			mountOptions = append(mountOptions, f)
		}
	}

	newVolumeContext := map[string]string{}

	for k, v := range req.VolumeContext {
		newVolumeContext[k] = v
	}

	var endpointMap map[string]string
	err = json.Unmarshal([]byte(req.VolumeContext["endpointMapJson"]), &endpointMap)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "NodeStageVolume: could not unmarshal endpoint map from volume context error: %v", err)
	}
	klog.V(4).Infof("NodeStageVolume: endpointMap %v", endpointMap)

	// Getting volume from statehub
	remoteVolume, err := d.statehub.GetVolumeByName(ctx, state, req.GetVolumeId())
	if err != nil {
		klog.Errorf("NodeStageVolume: failed to get volume %s in state %s from statehub", req.GetVolumeId(), state)
		return nil, status.Error(codes.Internal, err.Error())
	}

	var volumeLocation *openapi.VolumeLocation = nil
	// Get iscsi info for active location
	for lIndex := range remoteVolume.Locations {
		if remoteVolume.Locations[lIndex].Name == remoteVolume.GetActiveLocation() {
			volumeLocation = &remoteVolume.Locations[lIndex]
			break
		}
	}

	for k, v := range getPublishContext(endpointMap, volumeID, d.metadata.GetSubnet(), volumeLocation) {
		newVolumeContext[k] = v
	}

	req.VolumeContext = newVolumeContext

	klog.V(4).Infof("NodeStageVolume: VolumeContext %v", req.GetVolumeContext())

	iscsiInfo, err := iscsi.GetISCSIInfo(req)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	diskMounter := iscsi.GetISCSIDiskMounter(iscsiInfo, req)

	util := &iscsi.ISCSIUtil{}

	devicePath, err := util.AttachDisk(*diskMounter)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	// The disk is attached, now we mount and format if needed

	deviceMounter := mounter.GetDeviceMounterStage(req, devicePath)
	deviceMounter.EventLogger = d.statehub.SendEvent
	if remoteVolume.Format == nil {
		deviceMounter.ShouldFormat = true
	}
	err = mounter.MountStaging(deviceMounter, ctx)
	if err != nil {
		klog.Errorf("NodeStageVolume: failed to mount volume %s in path %s", req.GetVolumeId(), target)
		// First we unmount the volume to avoid data corraption
		deviceUmounter := mounter.GetDeviceUmounter()
		umounterr := mounter.UmountStaging(deviceUmounter, target)
		if umounterr != nil {
			return nil, status.Errorf(codes.Internal, "Could not unmount %q while recovering from mounting error.\n mount error: %v\numount error: %v", target, err, umounterr)
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	if remoteVolume.Format == nil {
		err = d.statehub.FromatVolume(ctx, state, req.GetVolumeId())

		if err != nil {
			klog.Errorf("NodeStageVolume: failed to set volume %s in state %s as formated", req.GetVolumeId(), state)
			// First we unmount the volume to avoid data corraption
			deviceUmounter := mounter.GetDeviceUmounter()
			umounterr := mounter.UmountStaging(deviceUmounter, target)
			if umounterr != nil {
				return nil, status.Errorf(codes.Internal, "Could not unmount %q while recovering from set format error.\n set format error: %v\numount error: %v", target, err, umounterr)
			}
		}
	}

	return &csi.NodeStageVolumeResponse{}, nil
}

func (d *nodeService) NodeUnstageVolume(ctx context.Context, req *csi.NodeUnstageVolumeRequest) (*csi.NodeUnstageVolumeResponse, error) {
	klog.V(4).Infof("NodeUnstageVolume: called with args %+v", *req)
	volumeID := req.GetVolumeId()
	if len(volumeID) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Volume ID not provided")
	}

	targetPath := req.GetStagingTargetPath()
	if len(targetPath) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Staging target not provided")
	}

	if ok := d.inFlight.Insert(volumeID); !ok {
		return nil, status.Errorf(codes.Aborted, VolumeOperationAlreadyExists, volumeID)
	}
	defer func() {
		klog.V(4).Infof("NodeUnStageVolume: volume=%q operation finished", volumeID)
		d.inFlight.Delete(volumeID)
	}()

	// Now we unmount the volume from staging target
	deviceUmounter := mounter.GetDeviceUmounter()
	err := mounter.UmountStaging(deviceUmounter, targetPath)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Could not unmount %q: %v", targetPath, err)
	}

	// Lets disconnect from iscsi
	diskUnmounter := iscsi.GetISCSIDiskUnmounter(req)
	iscsiutil := &iscsi.ISCSIUtil{}
	err = iscsiutil.DetachDisk(*diskUnmounter, targetPath)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Could not disconnect iscsi %q: %v", targetPath, err)
	}

	return &csi.NodeUnstageVolumeResponse{}, nil
}

func (d *nodeService) NodeExpandVolume(ctx context.Context, req *csi.NodeExpandVolumeRequest) (*csi.NodeExpandVolumeResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}

func (d *nodeService) NodePublishVolume(ctx context.Context, req *csi.NodePublishVolumeRequest) (*csi.NodePublishVolumeResponse, error) {
	klog.V(4).Infof("NodePublishVolume: called with args %+v", *req)
	volumeID := req.GetVolumeId()
	if len(volumeID) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Volume ID not provided")
	}

	source := req.GetStagingTargetPath()
	if len(source) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Staging target not provided")
	}

	target := req.GetTargetPath()
	if len(target) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Target path not provided")
	}

	volCap := req.GetVolumeCapability()
	if volCap == nil {
		return nil, status.Error(codes.InvalidArgument, "Volume capability not provided")
	}

	if volCap.GetBlock() != nil && volCap.GetMount() != nil {
		return nil, status.Error(codes.InvalidArgument, "cannot have both block and mount access type")
	}
	if volCap.GetBlock() == nil && volCap.GetMount() == nil {
		return nil, status.Error(codes.InvalidArgument, "cannot have neither block and mount access type")
	}

	if !isValidVolumeCapabilities([]*csi.VolumeCapability{volCap}) {
		return nil, status.Error(codes.InvalidArgument, "Volume capability not supported")
	}

	if ok := d.inFlight.Insert(volumeID); !ok {
		return nil, status.Errorf(codes.Aborted, VolumeOperationAlreadyExists, volumeID)
	}
	defer func() {
		klog.V(4).Infof("NodePublishVolume: volume=%q operation finished", volumeID)
		d.inFlight.Delete(volumeID)
	}()

	deviceMounter := mounter.GetDeviceMounterPublish(req, source, target)
	err := mounter.MountPublish(deviceMounter)
	if err != nil {
		klog.Errorf("NodePublishVolume: failed to mount volume %s in path %s", req.GetVolumeId(), target)
		// First we unmount the volume to avoid data corraption
		deviceUmounter := mounter.GetDeviceUmounter()
		umounterr := mounter.UmountStaging(deviceUmounter, target)
		if umounterr != nil {
			return nil, status.Errorf(codes.Internal, "Could not unmount %q while recovering from mounting error.\n mount error: %v\numount error: %v", target, err, umounterr)
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &csi.NodePublishVolumeResponse{}, nil
}

func (d *nodeService) NodeUnpublishVolume(ctx context.Context, req *csi.NodeUnpublishVolumeRequest) (*csi.NodeUnpublishVolumeResponse, error) {
	klog.V(4).Infof("NodeUnpublishVolume: called with args %+v", *req)
	volumeID := req.GetVolumeId()
	if len(volumeID) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Volume ID not provided")
	}

	target := req.GetTargetPath()
	if len(target) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Target path not provided")
	}
	if ok := d.inFlight.Insert(volumeID); !ok {
		return nil, status.Errorf(codes.Aborted, VolumeOperationAlreadyExists, volumeID)
	}
	defer func() {
		klog.V(4).Infof("NodeUnPublishVolume: volume=%q operation finished", volumeID)
		d.inFlight.Delete(volumeID)
	}()

	klog.V(4).Infof("NodeUnpublishVolume: unmounting %s", target)

	deviceUmounter := mounter.GetDeviceUmounter()
	err := mounter.UmountStaging(deviceUmounter, target)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Could not unmount %q error: %v", target, err)
	}

	return &csi.NodeUnpublishVolumeResponse{}, nil
}

func (d *nodeService) NodeGetVolumeStats(ctx context.Context, req *csi.NodeGetVolumeStatsRequest) (*csi.NodeGetVolumeStatsResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}

func (d *nodeService) NodeGetCapabilities(ctx context.Context, req *csi.NodeGetCapabilitiesRequest) (*csi.NodeGetCapabilitiesResponse, error) {
	klog.V(4).Infof("NodeGetCapabilities: called with args %+v", *req)
	var caps []*csi.NodeServiceCapability
	for _, cap := range nodeCaps {
		c := &csi.NodeServiceCapability{
			Type: &csi.NodeServiceCapability_Rpc{
				Rpc: &csi.NodeServiceCapability_RPC{
					Type: cap,
				},
			},
		}
		caps = append(caps, c)
	}
	return &csi.NodeGetCapabilitiesResponse{Capabilities: caps}, nil
}

func (d *nodeService) NodeGetInfo(ctx context.Context, req *csi.NodeGetInfoRequest) (*csi.NodeGetInfoResponse, error) {
	klog.V(4).Infof("NodeGetInfo: called with args %+v", *req)

	segments := map[string]string{
		statehubTypes.StatehubVnetTopologyLabel:          d.metadata.GetVnet(),
		statehubTypes.StatehubSubnetTopologyLabel:        d.metadata.GetSubnet(),
		statehubTypes.StatehubRegionTopologyLabel:        d.metadata.GetRegion(),
		statehubTypes.StatehubProviderTopologyLabel:      string(d.metadata.GetCloudProvider()),
		statehubTypes.StatehubProviderScopeTopologyLabel: string(d.metadata.GetProviderScope()),
		statehubTypes.StatehubResourceGroupTopologyLabel: string(d.metadata.GetResourceGroup()),
		statehubTypes.StatehubStatusTopologyLabel:        "enabled",
	}

	topology := &csi.Topology{Segments: segments}

	return &csi.NodeGetInfoResponse{
		NodeId:             d.metadata.GetInstanceID(),
		MaxVolumesPerNode:  d.getVolumesLimit(),
		AccessibleTopology: topology,
	}, nil
}

// getVolumesLimit returns the limit of volumes that the node supports
func (d *nodeService) getVolumesLimit() int64 {
	if d.driverOptions.volumeAttachLimit >= 0 {
		return d.driverOptions.volumeAttachLimit
	}
	return defaultMaxVolumes
}

// hasMountOption returns a boolean indicating whether the given
// slice already contains a mount option. This is used to prevent
// passing duplicate option to the mount command.
func hasMountOption(options []string, opt string) bool {
	for _, o := range options {
		if o == opt {
			return true
		}
	}
	return false
}

func getPublishContext(secrets map[string]string, volumeName string, subnet string, volumeLocation *openapi.VolumeLocation) map[string]string {

	var iqn string
	iqn = fmt.Sprint("iqn.2021-07.io.statehub:", volumeName)
	var (
		discoveryCHAPAuth = "false"
		sessionCHAPAuth   = "false"
		chap_user         = ""
		chap_password     = ""
	)
	if volumeLocation != nil {
		if volumeLocation.GetIscsi().Iqn != "" {
			iqn = volumeLocation.GetIscsi().Iqn
		}

		if volumeLocation.GetIscsi().Chap.GetUser() != "" {
			// discoveryCHAPAuth = "true"
			sessionCHAPAuth = "true"
			chap_user = volumeLocation.GetIscsi().Chap.GetUser()
			chap_password = volumeLocation.GetIscsi().Chap.GetSecret()
			klog.V(4).Infof("getPublishContext recived chap auth user %s", chap_user)
		} else {
			klog.V(4).Infof("getPublishContext no chap auth recived")
		}
	}

	klog.V(4).Infof("getPublishContext recived secrets %v", secrets)
	klog.V(4).Infof("getPublishContext using subnet %s with targetPortal %s", subnet, secrets[subnet])
	replicaPublishContext := map[string]string{
		"targetPortal":      secrets[subnet],
		"iqn":               iqn,
		"lun":               "0",
		"discoveryCHAPAuth": discoveryCHAPAuth,
		"sessionCHAPAuth":   sessionCHAPAuth,
		"chapUser":          chap_user,
		"chapPassword":      chap_password,
	}

	return replicaPublishContext
}
